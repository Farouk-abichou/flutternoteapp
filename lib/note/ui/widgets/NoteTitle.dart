

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NoteTitle extends StatelessWidget {
  final TextEditingController _titleController;

  const NoteTitle({
    Key key,
    @required TextEditingController titleController,
  })  : _titleController = titleController,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter the note title';
        }
        return null;
      },
      controller: _titleController,
      style: const TextStyle(color: Color(0xffdadada)),
      decoration: const InputDecoration(
        labelStyle: TextStyle(color: Colors.black38),
        border: OutlineInputBorder(),
        labelText: 'Note Title',

      ),
    );
  }
}
