
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NoteBody extends StatelessWidget {

  const NoteBody({
    Key key,
    @required TextEditingController contentController,
  })  : _contentController = contentController,
        super(key: key);

  final TextEditingController _contentController;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, bottom: 20),
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return 'Please enter the note body!';
          }
          return null;
        },
        maxLines: 50,
        controller: _contentController,
        style: const TextStyle(color: Color(0xffbbbbbb)),
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
        ),
      ),
    );
  }
}

