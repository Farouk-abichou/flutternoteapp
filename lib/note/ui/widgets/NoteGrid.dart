import 'package:flutter/material.dart';
import '../bloc/note_state.dart';
import '../edit_note.dart';
import 'note_card.dart';

class NoteGrid extends StatelessWidget {
  final YourNotesState state;
  const NoteGrid({Key key, this.state}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
          padding: const EdgeInsets.all(15.0),
          child: GridView.builder(
              shrinkWrap: true,
              itemCount: state.notes.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 0.72,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
              ),

              itemBuilder: (context, index) {
                final note = state.notes[index];
                return InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (BuildContext context) {
                          return EditNoteScreen(
                            note: note,
                            index: index,
                          );
                        }));
                  },
                  child: NoteCard(
                    title: note.title,
                    content: note.content,
                  ),
                );
              }
          ),
    );

  }}
