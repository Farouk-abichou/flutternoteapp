
import 'package:flutter/material.dart';

class NoteCard extends StatelessWidget {

  final String title;
  final String content;

  const NoteCard({Key key, this.title, this.content}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const Color black = Color(0xff0a0718);
    return Padding(
      padding: const EdgeInsets.all(5),
      child: Container(
        decoration: BoxDecoration(
          gradient: const LinearGradient(
            begin : Alignment.topCenter,
            end : Alignment.bottomLeft,
            colors: [
              Color(0xfff1d6f1),
              Color(0xffd6c8e7),
              Color(0xfff5e4f5),
            ],

          ),
          borderRadius: BorderRadius.circular(16),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 20, bottom: 10, left: 5, right: 5),
          child: Column(
            children: [
              Text(
                title,
                style: const TextStyle(
                    color: black,
                    letterSpacing: 1,
                    fontSize: 20,
                    fontWeight: FontWeight.w400),
              ),
              const SizedBox(
                height: 10,
              ),

              const SizedBox(
                height: 10,
              ),
              Expanded(
                child: Text(
                  content,
                  style: const TextStyle(
                      color: black,
                      fontSize: 14,
                      wordSpacing: 1.25,
                      letterSpacing: 1),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}