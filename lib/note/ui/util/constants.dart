

import 'dart:ui';

const whiteColor = Color(0xffffffff);
const lightWhitePinkColor = Color(0xffefe1ef);
const lightPinkColor = Color(0xffe7c8e7);
const pinkColor = Color(0xffc266a7);
const blueColor = Color(0xff52489f);
const purpleColor = Color(0xff4c2f6f);
const darkPurpleColor = Color(0xff0d0b33);
const blackColor = Color(0xff000000);
