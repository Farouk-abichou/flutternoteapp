import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../domain/model/Note.dart';


abstract class NoteState extends Equatable {
  @override
  List<Object> get props => [];
}

class NoteInitial extends NoteState {

}

class NotesLoading extends NoteState {}

class EditNotesState extends NoteState {
  final Note note;

  EditNotesState({@required this.note});

}
class YourNotesState extends NoteState {
  final List<Note> notes;

  YourNotesState({@required this.notes});

}
class NewNoteState extends NoteState {}