import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/repository/NoteRepositoryImpl.dart';
import '../../domain/model/Note.dart';
import 'note_event.dart';
import 'note_state.dart';

class NoteBloc extends Bloc<NoteEvent, NoteState> {

  final NoteRepositoryImpl _noteRepository;
  List<Note> _notes = [];
  NoteBloc(this._noteRepository) : super(NoteInitial());


  Future<void> _getNotes() async {
    _notes =  _noteRepository.getFullNote();
  }

  Future<void> _addToNotes(String title, String content) async {
    await _noteRepository.addToBox(Note(title,content));
    await _getNotes();
  }

  @override
  Stream<NoteState> mapEventToState(NoteEvent event) async* {
    if (event is NoteInitialEvent) {
      yield* _mapInitialEventToState();
    }
    if (event is NoteAddEvent) {
      yield* _mapNoteAddEventToState(
          title: event.title, content: event.content);
    }
  }


  // Stream Functions
  Stream<NoteState> _mapInitialEventToState() async* {
    await _getNotes();
    yield YourNotesState(notes: _notes);
  }


  Stream<NoteState> _mapNoteAddEventToState(
      {String title, String content}) async* {
    await _addToNotes(title,content);
    yield YourNotesState(notes: _notes);
  }



}