import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_notes_app/note/ui/util/constants.dart';
import 'bloc/note_bloc.dart';
import 'bloc/note_state.dart';
import 'widgets/NoteGrid.dart';
import 'edit_note.dart';

class NotesScreen extends StatelessWidget {
  const NotesScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [whiteColor, lightWhitePinkColor])),
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: darkPurpleColor,
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (BuildContext context) {
                  return const EditNoteScreen(


                  );
                }));
          },
          child: const Icon(Icons.add, color: lightWhitePinkColor),
        ),
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: const Text(
            'Notes',
            style: TextStyle(color: darkPurpleColor),
          ),
          actions: [
            IconButton(
              icon: const Icon(
                Icons.notes,
                color:darkPurpleColor,
              ),
              onPressed: () {

              },
            )
          ],
        ),
        body:  SingleChildScrollView(
            child:  Column(
            children: [
              BlocBuilder<NoteBloc, NoteState>(
                builder: (context, state) {
                  if (state is NoteInitial) {
                    return const Center(
                      child: Text("No Notes")
                    );
                  }
                  if (state is YourNotesState) {
                    return NoteGrid(state: state);
                  }
                  if (state is NotesLoading) {
                    return Container();
                  } else {
                    return Container();
                  }
                },
              ),
            ],
            ),
        ),
      ),
    );
  }
}
