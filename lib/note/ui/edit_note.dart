import 'package:flutter/material.dart';
import 'package:flutter_notes_app/note/ui/util/constants.dart';
import '../domain/model/Note.dart';
import 'bloc/note_event.dart';
import 'widgets/NoteBody.dart';
import 'widgets/NoteTitle.dart';

class EditNoteScreen extends StatefulWidget {
  final Note note;
  final int index;
  const EditNoteScreen({Key key, this.note, this.index, }) : super(key: key);

  @override
  EditNoteScreenState createState() => EditNoteScreenState();
}

class EditNoteScreenState extends State<EditNoteScreen> {

  @override
  Widget build(BuildContext context) {
    String title = widget.note != null ? widget.note.title : "";
    String content = widget.note != null ? widget.note.content : "";
    final _titleController = TextEditingController(text: title);
    final _contentController = TextEditingController(text: content);


    return Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [whiteColor, lightWhitePinkColor])),
        child: Scaffold(
            floatingActionButton: FloatingActionButton.extended(
              backgroundColor: darkPurpleColor,
              icon: Icon(Icons.add),
              label: Text( 'ADD' ),
              onPressed: () {
                  NoteAddEvent(
                      title: _titleController.text,
                      content: _contentController.text);
                  Navigator.pop(context);
              },
            ),

          backgroundColor: Colors.transparent,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: whiteColor,
            title: const Text(
              'New Note' ,
              style: TextStyle(color: darkPurpleColor),
            ),
            leading: IconButton(icon: const Icon(Icons.arrow_back, color: darkPurpleColor,),
              onPressed: (){
              Navigator.pop(context);
            },),

          ),

          body: SingleChildScrollView(
          child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    NoteTitle(titleController: _titleController),
                    const SizedBox(
                      height: 20,
                    ),
                    NoteBody(contentController: _contentController,)
                    
                  ],
                ),
            ),
          ),
        ),
    );
  }
}
