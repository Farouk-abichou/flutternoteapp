

import '../model/Note.dart';

abstract class NoteRepository {

  List<Note> getFullNote();

  Future<void> addToBox(Note note);

}