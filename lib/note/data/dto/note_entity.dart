import 'package:hive/hive.dart';
part 'note_entity.g.dart';

@HiveType(typeId: 1)
class NoteEntity {
  @HiveField(0)
  final String title;
  @HiveField(1)
  final String content;

  NoteEntity({this.title, this.content});

}