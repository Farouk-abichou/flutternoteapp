


import 'package:flutter_notes_app/note/data/dto/note_entity.dart';

import '../../domain/model/Note.dart';
import 'note_database/note_database.dart';

class NoteLocalDataSource{

  Future<void> addToBox(Note note) async {
    final box = await NoteDatabase().noteBox();
    await box.add(note);
  }



  Future<List<NoteEntity>> getFullNote() async {
    final box = await NoteDatabase().noteBox();
    return box.values.toList();
  }


}