import 'package:hive/hive.dart';

import 'dart:async';

import '../../dto/note_entity.dart';


class NoteDatabase {
  final String _boxName = "Note";

  Future<Box> noteBox() async {
    var box = await Hive.openBox<NoteEntity>(_boxName);
    return box;
  }

}


