
import '../../domain/model/Note.dart';
import '../../domain/repository/NoteRepository.dart';
import '../dto/note_entity.dart';
import '../local/note_local_data_source.dart';

class NoteRepositoryImpl implements NoteRepository{
  @override
  Future<void> addToBox(Note note) async {
    NoteLocalDataSource().addToBox(note);
  }

  @override
  List<Note> getFullNote()  {


    Future<List<NoteEntity>> entityNotes = NoteLocalDataSource().getFullNote();
    var map = {};

    entityNotes.then((value) =>
        value.forEach((element) {
          map[element.title] = {'title' : element.title,'content' : element.content};
        })
    );

    List<Note> notes = [];
    for (var e in map.entries) {
      notes.add(Note( e.value['title'], e.value['content']));
    }

    return notes;
  }

}