import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_notes_app/note/data/repository/NoteRepositoryImpl.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'note/data/dto/note_entity.dart';
import 'note/ui/bloc/note_bloc.dart';
import 'note/ui/bloc/note_event.dart';
import 'note/ui/notes_screen.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter<NoteEntity>(NoteAdapter());

  runApp(BlocProvider(
    create: (context) => NoteBloc(NoteRepositoryImpl()),
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<NoteBloc>(context).add(NoteInitialEvent());
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
      ),
      home: const NotesScreen(),
    );
  }
}
